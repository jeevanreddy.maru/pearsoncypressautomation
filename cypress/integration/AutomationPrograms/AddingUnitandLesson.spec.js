import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Courseware from '../PageObjects/Courseware';

const Ip = new LoginPage();
const Hp = new HomePage();
const CW = new Courseware();

describe('Create a Course with Lesson', () => {
    before(() => {
        cy.fixture('example').then(function (data) {
        this.data = data;
        cy.visit('/');
        cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
        cy.title().should('include', 'Pearson');
        Ip.fillEmail(this.data.email);
        Ip.fillPassword(this.data.password);
        Ip.submit();
        })
    })
    it('Switch to QA Sandbox', function() {
        cy.get("i[id='menu']").click();
        cy.get("#keyboard_arrow_up").click();
        //cy.get("div[class='styles__SubscriptionContent-soyzm6-5 kHuJGO']").should('contain','QA Sandbox').click();
        cy.xpath("//span[contains(normalize-space(),'QA Sandbox')]").should('contain','QA Sandbox').click();
        cy.wait(3000);
    })

    it('Verify Course is created', () => {
        Hp.navigateToCourseware();
        cy.xpath("//div[contains(text(),'Cypressstest')]").should('be.visible').click();
        cy.contains('Content').should('be.visible').click();
        cy.get("button.styles__MenuItem-grhvnq-3.fKsWnT:nth-child(1) > div.styles__MenuListItemContent-grhvnq-4.dTDUeB:nth-child(1) > div")
        .should('be.visible').click();  
        cy.wait(1000); 
        cy.get("button:contains('Blank')").click();
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(6) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
        cy.xpath("//div[@class='styles__PluginsListContainer-gagfhv-0 lhTCws']//span[contains(text(),'Simple Course')]").click();
        //cy.get("select[name = 'pathwayTypes[1]']").select("LINEAR");
        cy.get("select").select("LINEAR").should('contain', "Linear");
        cy.contains('Continue').click();

    })
    it('Verify Unit is added to the course', () => {
        cy.wait(1000);
        cy.get("#add").click();
        cy.contains('Blank').click();
        cy.contains("Simple Unit").click();
        cy.get("select[name = 'pathwayTypes[0]']").select("FREE").should('contain', "Free");
        cy.contains('Continue').click();
    })
    it('Verify Lesson is added to the unit', () => {
        cy.get("#expand_more").click();
        //cy.get("button span").eq(3).click();
        cy.get("button[class='styles__ButtonStandard-sc-9phwpr-1 styles__ButtonFlat-sc-9phwpr-3 kDCyMu'] i[id='add']").click();
        cy.contains('Blank').click({force: true});
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(2) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
       })

       it('Verify Lesson is added to the unit with free pathway', () => {
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(2) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
        cy.xpath("//div[@class='styles__PluginsListContainer-gagfhv-0 lhTCws']//span[text('Standard Lesson']").click();
        cy.get("select[name = 'pathwayTypes[0]']").select("FREE").should('contain', "Free");
        cy.contains('Continue').click();
       })

     
    })
