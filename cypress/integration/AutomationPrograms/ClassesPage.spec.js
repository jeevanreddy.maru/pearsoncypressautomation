import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Courseware from '../PageObjects/Courseware';

const Ip = new LoginPage();
const Hp = new HomePage();
const Cp = new Courseware();

describe('Classes Page', () => {
    before(() => {
        cy.fixture('example').then(function (data) {
            this.data = data;
            cy.visit('/');
            cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
            cy.title().should('include', 'Pearson');
            Ip.fillEmail(this.data.email);
            Ip.fillPassword(this.data.password);
            Ip.submit();
        })
    })

    it('Switch to QA Sandbox', function()  {
        cy.get('#menu').click();
        cy.get("#keyboard_arrow_up").click();
        //cy.get("div[class='styles__SubscriptionContent-soyzm6-5 kHuJGO']").should('contain','QA Sandbox').click();
        cy.xpath("//span[contains(normalize-space(),'QA Sandbox')]").should('contain','QA Sandbox').click();
        cy.wait(3000);
    })

    it('Click on Classes module', function(){
        cy.get("#menu").click();
        cy.contains('Classes').should('contain', 'Classes').click();
        //cy.wait(10000);
    })

    it('Verifing Classes title', () => {
        cy.get("span[class='styles__Title-q6798b-2 hgdzZH']").should('contain', 'Classes');
    })
    it('Verifing Columns in Classes Page', () => {
        cy.get("div[class='styles__ListItemColumn-sc-2axawy-4 jNoQDB'] div[class='styles__ListHeaderItem-sc-2axawy-1 gJINPQ']").should('contain', 'Name');
        cy.get("div[class='styles__ListHeader-sc-2axawy-5 igkfik'] div[class='styles__ListItemColumn-sc-2axawy-4 gnxeOd']").should('contain', 'Date');
        cy.get("div[class='styles__ListHeader-sc-2axawy-5 igkfik'] div:nth-child(3) div:nth-child(1)").should('contain', 'Students');
        cy.get("div[class='styles__ListHeader-sc-2axawy-5 igkfik'] div:nth-child(4) div:nth-child(1)").should('contain', 'Lessons');
        cy.get("div[class='styles__ListItemColumn-sc-2axawy-4 hsrNed'] div[class='styles__ListHeaderItem-sc-2axawy-1 gJINPQ']").should('contain', 'Instructors');
    })
    it('Verifing Duplicate button is visible in Classes Page', () => {
    //cy.get('div.styles__ListItem-sc-2axawy-3.enhanceWithNavigation__StyledComponent-sc-1m0xhl7-0.jTxtQD:nth-child(1) > div.styles__ListItemColumn-sc-2axawy-4.hJSSYs:nth-child(6) > div.styles__CohortColumnItem-sc-1rcd3e5-2.iUoCaA:nth-child(1) > div.enhanceWithPopper__StyledManager-sc-1qmqau6-0.lcxDQQ:nth-child(1) > div:nth-child(1) > button.styles__ButtonStandard-sc-9phwpr-1.styles__ButtonGrey-sc-9phwpr-4.gGWRYC:nth-child(1) > span.styles__InnerButton-sc-9phwpr-5.blXHtF:nth-child(1) > i').click();
     cy.get("i[id='more_vert']").eq(0).click();

    })
     
    it('Click on duplicate button in Classes Page', () => {
    cy.get("div[class='styles__MenuListItemText-sc-1rcd3e5-9 iaexDx']").should('contain','Duplicate').click();
    })
    
    it('Verify user is able to see side menus ', () => {
        cy.contains('QATEST').should('contain','QATEST').click();
        cy.get("#menu").click();
        
    })

    it('Verify Enrollments,Course ware,Grades & Progress and settings is displayed in side menu',()=>{
        cy.contains('Enrollments').should('contain','Enrollments');
        cy.contains('Courseware').should('contain','Courseware');
        cy.contains('Grades & Progress').should('contain','Grades & Progress');
        cy.contains('Settings').should('contain','Settings');
    })
    // Enrollmentpage
    it('Verify user is able to see Enrollments Page ', () => {
        cy.contains('Enrollments').should('contain','Enrollments').click();  
        cy.get("span[class='InlineTextInput__NormalMode-sc-1m0jxex-0 erKQEb']").trigger('mouseover');
        
    })

    it('Verify popup on clicking  on instructor ', () => {
        cy.get("i[id='add']").eq(0).click();
        cy.get("div[class='styles__DialogOuter-rwkqr7-0 fYpdlV']").should('be.visible');
        
    })
    it('Verify Login username beside instructor ', () => {
        cy.get("div[class='styles__InitialsAvatar-sc-1bv089f-1 rQLPx']").should('be.visible');   
    })

    it('Verify Type a name to Invite beside instructor ', () => {
        cy.get("input[placeholder='Type a name to invite']").should('be.visible');
        
    })

    it('Verify dropdown beside username', () => {
        cy.get('#arrow_drop_down').should('be.visible').click();
        cy.wait(1000);
    })

    it('Verify Owner option checked', () => {
       cy.get("i[id='done']").should('be.visible').and('have.text', 'done');
    })

    it('Verify popup on clicking on EnrollStudents Btn', () => {
        cy.get("i[id='add']").eq(0).click();
        cy.contains('Enroll Students').should('be.visible').click();
        cy.get("div[class='styles__DialogHeader-rwkqr7-1 efGvIz']").should('be.visible'); 
    })

    it('Verify Auto Suggestion Username', () => {
        cy.get("input[placeholder='Type names or emails to enroll']").should('be.visible').type('QA');    
    })

    it('Verify username is selected ', () => {
        cy.contains('QA User 16 QA User 16').should('be.visible').click();  
    })

    it('Verify classname is editable ', () => {
        cy.get("span[class='InlineTextInput__NormalMode-sc-1m0jxex-0 erKQEb']").should('be.visible');
        
    })

    it('Verify classname is editable mode while clicking on white line', () => {
        cy.get("span[class='InlineTextInput__NormalMode-sc-1m0jxex-0 erKQEb']").should('be.visible').click();
        cy.wait(2000);
        
    })

    it('Verify classname is edited ', () => {
        cy.get("textarea[class='TextArea__FittedTextArea-sc-1ffw1xs-0 eIwXQt InlineTextInput__EditMode-sc-1m0jxex-1 hwzfHx']")
        .clear().type('QATEST');
        //cy.get("span[class='InlineTextInput__NormalMode-sc-1m0jxex-0 erKQEb']").type('QATEST');
        
    })
    
    // it('Verify user is able to see Courseware Page ', () => {
    //     cy.get("#menu").click();
    //     cy.contains('Courseware').should('contain','Courseware').click();  
    // })

    // it('Verify user is able to see Grades&Progress Page ', () => {
    //     cy.get("#menu").click();
    //     cy.contains('Grades & Progress').should('contain','Grades & Progress').click();
        
    // })

    // it('Verify user is able to see Settings Page ', () => {
    //     cy.get("#menu").click();
    //     cy.contains('Settings').should('contain','Settings').click();
        
    // })












})