import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Classes from '../PageObjects/Classes';

const Ip = new LoginPage();
const Hp = new HomePage();
const CLP = new Classes();
describe('Create a Project', function() {
        before('Login to Pearson Project', function()  {
        cy.fixture('example').then(function (data) {
            this.data = data;
            cy.visit('/');
            cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
            cy.title().should('include', 'Pearson');
            Ip.fillEmail(this.data.email);
            Ip.fillPassword(this.data.password); 
            Ip.submit();
            cy.wait(3000);
         })
   })
   it('Switch to QA Sandbox', function() {
    cy.get("i[id='menu']").click();
    cy.get("#keyboard_arrow_up").click();
    //cy.get("div[class='styles__SubscriptionContent-soyzm6-5 kHuJGO']").should('contain','QA Sandbox').click();
    cy.xpath("//span[contains(normalize-space(),'QA Sandbox')]").should('contain','QA Sandbox').click();
    cy.wait(3000);
    })
    it('Click on courseware', function()  {
        cy.get("#menu").click();
        cy.contains('Courseware').should('contain', 'Courseware').click();
    })

    it('Verify courseware title', function()  {
       cy.get("span[class='styles__Title-q6798b-2 hgdzZH']").should('contain', 'Courseware');
    })

    it('Verify Theme is displayed in the username drop down', function()  {
        //cy.get("span[class='styles__Title-q6798b-2 hgdzZH']div[class='styles__InitialsAvatar-sc-1bv089f-1 rQLPx styles__AvatarWrapper-sc-1riils1-0 hFDsA']").click();
        cy.get('.enhanceWithPopper__StyledManager-sc-1qmqau6-0 > :nth-child(1) > .styles__InitialsAvatar-sc-1bv089f-1').click();
        cy.get("button:nth-child(1) div:nth-child(1) div:nth-child(1)").should('contain', 'Theme');
     })

     it('Verify user is able to click on theme button', function()  {
        cy.get("button:nth-child(1) div:nth-child(1) div:nth-child(1)").should('contain', 'Theme').click();
     })

     it('Verify user is able to see the options', function()  {
        //cy.get("button.styles__MenuItem-grhvnq-3.fKsWnT.styles__MenuListItem-xca5v5-2.hEUcJP:nth-child(2) > div").should('contain', 'Dark');
        //cy.get(':nth-child(2) > .styles__MenuListItemContent-grhvnq-3').should('contain', 'Dark');
        //cy.get("button[class='styles__MenuItem-grhvnq-3 jkhcjM styles__MenuListItem-xca5v5-2 hEUcJP'] div[class='styles__MenuListItemText-xca5v5-3 fKiovn']").should('contain', 'Light');
        cy.contains('Dark').should('be.visible');
       // cy.wait(1000);
        cy.contains('Light').should('be.visible');
        cy.contains('Extra light').should('be.visible');
     })

     it('Verify user is able to click on back button', function()  {
        cy.get("#chevron_left").should('be.visible').click();
     })

     it('Verify Dark theme', function()  {
        cy.get("button:nth-child(1) div:nth-child(1) div:nth-child(1)").should('contain', 'Theme').click();
        cy.contains('Dark').should('contain', 'Dark').click();
        cy.wait(1000);
     })

     it('Verify Light theme', function()  {
        //cy.get("button[class='styles__MenuItem-grhvnq-3 jkhcjM styles__MenuListItem-xca5v5-2 hEUcJP'] div[class='styles__MenuListItemContent-grhvnq-4 dTDUeB']").should('contain', 'Theme').click();
        cy.contains('Light').should('contain', 'Light').click();
        cy.wait(1000);
     })

     it('Verify Extra Light theme', function()  {
        //cy.get("button.styles__MenuItem-grhvnq-3.fKsWnT.styles__MenuListItem-xca5v5-2.hEUcJP:nth-child(4) > div").should('contain', 'Theme').click();
        cy.contains('Extra light').should('contain', 'Extra light').click();
        cy.wait(1000);
     })
    // Project Creation

    it('Verify New project is visible', function()  {
        cy.get('.enhanceWithPopper__StyledManager-sc-1qmqau6-0 > :nth-child(1) > .styles__InitialsAvatar-sc-1bv089f-1').click();
        cy.get("button[class='styles__ButtonStandard-sc-9phwpr-1 styles__ButtonPrimary-sc-9phwpr-2 cKAXfm'] span[class='styles__InnerButton-sc-9phwpr-5 blXHtF']")
        .should('contain', 'New Project');  
     })

     it('Verify create project window is displayed', function()  {
      cy.contains('New Project').should('contain', 'New Project').click();
      cy.get("div[class='styles__ModalHeader-bq4wgc-3 hdwagA']").should('be.visible');
    })

    it('Verify project name field is displayed', function()  {
        cy.get("input[placeholder='E.g. Biology']").should('be.visible');
    })

      it('Verify user is able to click on close button', function()  {
        cy.get("#close").should('be.visible').click();
    })
    it('Verify user is able to create a project', function()  {
        cy.get('#add').should('be.visible').click();
        cy.get("input[placeholder='E.g. Biology']").should('be.visible').type('cypressautomation');
        cy.contains('Create project').click();
        cy.wait(1000);
    })

    it('Verify user is able to click on existing project', function()  {
        cy.xpath("//div[contains(text(), 'Cypressstest') and contains(@class,'styles__ProjectCardTitle-sc-121nexw-4 ihVjMz')]").click();
        //cy.get('#infinite-scroll-list').scrollTo(0, 500);
        cy.wait(2000);
        //cy.contains('Content').should('be.visible').click();
    })

    it('Verify user is able to see content button', function()  {
        cy.contains('Content').should('be.visible');
    })

    it('Verify user is able to see course and lesson options', function()  {
        cy.contains('Content').should('be.visible').click();
        cy.contains('Course').should('be.visible');
        cy.contains('Lesson').should('be.visible');
    })

    it('Verify user is able to click on course option', function()  {
        cy.get("button.styles__MenuItem-grhvnq-3.fKsWnT:nth-child(1) > div.styles__MenuListItemContent-grhvnq-4.dTDUeB:nth-child(1) > div")
        .should('be.visible').click();  
        cy.wait(1000); 

    })

    it('Verify user is able to click on lesson option', function()  {
        cy.get("#close").should('be.visible').click();
        cy.contains('Content').should('be.visible').click();
        cy.contains('Lesson').should('be.visible').click();
        cy.wait(1000);
    })



})