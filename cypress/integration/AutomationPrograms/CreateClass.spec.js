import 'cypress-xpath'
import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Classes from '../PageObjects/Classes';

const Ip = new LoginPage();
const Hp = new HomePage();
const CLP = new Classes();
describe('Create a Class and Deploy Course', function() {
        before('Login to Pearson Project', function()  {
        cy.fixture('example').then(function (data) {
            this.data = data;
            cy.visit('/');
            cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
            cy.title().should('include', 'Pearson');
            Ip.fillEmail(this.data.email);
            Ip.fillPassword(this.data.password);
            Ip.submit();
         })
   })
    it('Switch to QA Sandbox', function()  {
     cy.get("i[id='menu']").click();
     cy.get("#keyboard_arrow_up").click();
    //cy.get("div[class='styles__SubscriptionContent-soyzm6-5 kHuJGO']").should('contain','QA Sandbox').click();
     cy.xpath("//span[contains(normalize-space(),'QA Sandbox')]").should('contain','QA Sandbox').click();
     cy.wait(3000);
    })
    it('Click on Classes module', function(){
        Hp.navigateToClasses();
    })
    it('Enter Class Name', function() {
        CLP.fillClassName('Math 103');
    })
    it('Select the Start Date', function() {
        cy.get("input[type='button']").click();
        cy.get("abbr[aria-label='November 10, 2020']").click();
    })
    it('Enter Product ID', function() {
        CLP.fillProductID('TestID1');
    })
    it('Select Open enrollment radio button', function() {
        cy.contains('Open enrollment').click();
    })
    it('Click on Create Class button', function() {
        cy.contains("Create class").should('be.visible').click();
        cy.wait(1000);
    })
   // it('deploy Course to Class', function() {
      //  CLP.deployCourseToClass();
//})
})
