import 'cypress-xpath'
describe('Test Suite', () => {
    it('Creating class', () => {
        cy.visit('https://workspace-bronte-qaint.pearson.com/');
        cy.get("input[type='email']").type("qaint.user011@pearson.com");
        cy.get("input[type='password']").type("qaint.user011");
        cy.contains('Login').click();
        cy.get('#menu').click();
        cy.wait(1000);
        cy.get("#keyboard_arrow_up").click();
        cy.wait(1000);
        cy.contains("QA Sandbox").click();
        cy.get('#menu').click();
        cy.wait(1000);
        cy.contains('Classes').click();
        cy.get("#add").click();
        cy.wait(1000);
        cy.get('#className-input-3').type('Math 103');
        cy.get("#startDate-input-4").click();
        cy.get("abbr[aria-label='November 12, 2020']").click();
        cy.get('#pearsonProductIdPpid-input-5').type('TestID1');
        cy.contains('Open enrollment').click();
        cy.contains("Create class").click();
        cy.wait(1000);
        cy.contains('Math 103').click();
        cy.wait(1000);
        cy.contains('New Courseware').click();
        cy.contains('QAProject').click();
        cy.wait(1000);
        cy.contains('Next').click();
        cy.wait(1000);
        cy.contains('Slideshow Lesson').click();
        cy.wait(1000);
        cy.contains('Next').click();
        cy.wait(1000);
        cy.contains('Deploy').click();
        cy.wait(1000);
        cy.get('#more_vert').click();
        cy.wait(1000);
        cy.contains('Update Deployment').click();
    })
})
