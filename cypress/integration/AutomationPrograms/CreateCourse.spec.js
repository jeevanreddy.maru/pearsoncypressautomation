//Adding Unit and Lesson to it

import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Courseware from '../PageObjects/Courseware';

const Ip = new LoginPage();
const Hp = new HomePage();
const CW = new Courseware();

describe('Create a Course with Lesson', () => {
    before(() => {
        cy.fixture('example').then(function (data) {
        this.data = data;
        cy.visit('/');
        cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
        cy.title().should('include', 'Pearson');
        Ip.fillEmail(this.data.email);
        Ip.fillPassword(this.data.password);
        Ip.submit();
        })
    })
    it('Switch to QA Sandbox', function()  {
        Hp.navigateToQASandbox();
    })
    it('Verify Course is created', () => {
        Hp.navigateToCourseware();
        cy.wait(1000);
        cy.contains('New Project').click();
        //cy.get("i[id='add']").click();
        //cy.get("button div").eq(1).click();
        cy.wait(1000);
        cy.get("input[name='projectName']").click();
        cy.wait(1000);
        cy.get("input[name='projectName']").type("SampleAutomation");
        cy.wait(1000);
        cy.contains('Create project').click();
        cy.wait(1000);
        cy.contains('Content').click();
        cy.wait(2000);
        cy.xpath("//div[@class='styles__MenuListItemText-sc-17ag5ik-1 hOpRRH'and contains(text(),'Course')]").click();
        cy.wait(1000);
        cy.contains('Blank').click();
        //cy.get("button:contains('Blank')").click();
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(6) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
        cy.xpath("//div[@class='styles__PluginsListContainer-gagfhv-0 lhTCws']//span[contains(text(),'Simple Course')]").click();
        //cy.get("select[name = 'pathwayTypes[1]']").select("LINEAR");
        cy.get("select").select("LINEAR").should('contain', "Linear");
        cy.contains('Continue').click();

    })
    it('Verify Unit is added to the course', () => {
        cy.wait(1000);
        cy.get("#add").click();
        cy.contains('Blank').click();
        cy.contains("Simple Unit").click();
        cy.get("select[name = 'pathwayTypes[0]']").select("FREE").should('contain', "Free");
        cy.contains('Continue').click();
    })
    it('Verify Lesson is added to the unit', () => {
        cy.get("#expand_more").click();
        //cy.get("button span").eq(3).click();
        cy.get("button[class='styles__ButtonStandard-sc-9phwpr-1 styles__ButtonFlat-sc-9phwpr-3 kDCyMu'] i[id='add']").click();
        cy.contains('Blank').click({force: true});
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(2) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
       })

       it('Verify Lesson is added to the unit with free pathway', () => {
        //cy.get('body.fontLoaded:nth-child(2) div.styles__Wrapper-sc-1eeplaz-0.fOPuoW div.styles__FullHeightWrapper-bobpoo-2.cMmufa div.styles__PluginCollection-bobpoo-0.hjUMIi div.styles__PluginsListContainer-gagfhv-0.lhTCws button.styles__PluginBox-gagfhv-1.ffRJvH:nth-child(2) div.styles__PluginBoxHeader-gagfhv-2.jhXzqM > span.styles__PluginBoxTitle-gagfhv-3.xSOgI:nth-child(2)').click();
        cy.xpath("//div[@class='styles__PluginsListContainer-gagfhv-0 lhTCws']//span[text()='Standard Lesson']").click();
        cy.get("select[name = 'pathwayTypes[0]']").select("FREE").should('contain', "Free");
        cy.contains('Continue').click();
       })

     
    })

    


