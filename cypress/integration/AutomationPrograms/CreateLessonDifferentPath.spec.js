//Code for Creating lesson with different pathways:
import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';


const Ip = new LoginPage();
const Hp = new HomePage();
//const Cp = new Courseware();

describe('Create a new Lesson', () => {
    before(() => {
        cy.fixture('example').then(function (data) {
        this.data = data;
        cy.visit('/');
        cy.url().should('include', 'https://workspace-bronte-dev.pearson.com');
        cy.title().should('include', 'Pearson');
        Ip.fillEmail(this.data.email);
        Ip.fillPassword(this.data.password);
        Ip.submit();
     })
    // it('Verifing new Lesson is created with free pathway', () => {
    //     Hp.navigateToCourseware();
    //     Cp.navigateToLesson();
    //     cy.get("button:contains('Blank')").click();
    //     cy.get("button div").eq(1).click();
    //     cy.get("select").select("FREE").should('contain', 'Free');
    //     cy.contains('Continue').click();
    // })
    // it('Verify new Lesson is created with Linear pathway', () => {
    //     Hp.navigateToCourseware();
    //     Cp.navigateToLesson();
    //     cy.get("button:contains('Blank')").click();
    //     cy.get("button div").eq(1).click();
    //     cy.get("select").select("LINEAR").should('contain', 'Linear');
    //     cy.contains('Continue').click();
    // })
    // it('Verify new Lesson is created with Graph pathway', () => {
    //     Hp.navigateToCourseware();
    //     Cp.navigateToLesson();
    //     cy.get("button:contains('Blank')").click();
    //     cy.get("button div").eq(1).click();
    //     cy.get("select").select("GRAPH").should('contain', 'Graph');
    //     cy.contains('Continue').click();
    // })
})
})