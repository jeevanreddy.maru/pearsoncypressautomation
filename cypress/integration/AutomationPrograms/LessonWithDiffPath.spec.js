//Code for Creating lesson with different pathways:

import LoginPage from '../PageObjects/LoginPage';
import HomePage from '../PageObjects/HomePage';
import Courseware from '../PageObjects/Courseware';
import CreateLesson from '../PageObjects/CreateLesson';

const Ip = new LoginPage();
const Hp = new HomePage();
const Cp = new Courseware();
const CL = new CreateLesson();

describe('Create a new Lesson', () => {
    beforeEach(() => {
        cy.fixture('example').then(function (data) {
            this.data = data;
            cy.visit('/');
            cy.url().should('include', 'https://workspace-bronte-qaint.pearson.com');
            cy.title().should('include', 'Pearson');
            Ip.fillEmail(this.data.email);
            Ip.fillPassword(this.data.password);
            Ip.submit();
        })
    })
    it('Verifing new Lesson is created with free pathway', () => {
        Hp.navigateToCourseware();
        CL.navigateToLesson();
        cy.wait(1000);
        cy.get("button:contains('Blank')").click();
       // cy.get("button div").eq(1).click();
        cy.wait(2000);
        cy.xpath("//div[@class='styles__PluginsListContainer-gagfhv-0 lhTCws']//span[contains(text(),'Stiched Lesson')]").click();
        cy.wait(1000);
        cy.get("select").select("FREE").should('contain', 'Free');
        cy.contains('Continue').click();
    })
    it('Verify new Lesson is created with Linear pathway', () => {
        Hp.navigateToCourseware();
        CL.navigateToLesson();
        cy.wait(1000);
        cy.get("button:contains('Blank')").click();
        //cy.get("button div").eq(1).click();
        cy.wait(3000);
        cy.xpath("//span[contains(text(),'Standard Lesson')]").click();
        cy.wait(1000);
        cy.get("select").select("LINEAR").should('contain', 'Linear');
        cy.contains('Continue').click();
    })
    it('Verify new Lesson is created with Graph pathway', () => {
        Hp.navigateToCourseware();
        CL.navigateToLesson();
        cy.wait(1000);
        cy.get("button:contains('Blank')").click();
        cy.wait(2000);
        //cy.get("button div").eq(1).click();
        cy.xpath("//span[contains(text(),'Pearson Lesson')]").click();
        cy.wait(1000);
        cy.get("select").select("GRAPH").should('contain', 'Graph');
        cy.contains('Continue').click();
    })
})
