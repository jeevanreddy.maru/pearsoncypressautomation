class Classes {
    navigateClasses() {
        cy.get('#menu').click();
        cy.wait(1000);
        cy.contains('Classes').click();
        cy.get("#add").click();
        cy.wait(1000);
    }
    fillClassName(Value) {
        const field = cy.get('#className-input-3')
       field.type(Value);
       return this;
        }
    fillProductID(Value) {
        const field = cy.get("input[placeholder='Eg. x-urn:revel:aeb7fa90-b134-11ea-9f4d-a5b246f9138c']")
      field.type(Value);
        return this;
    }
       deployCourseToClass() {
        cy.contains('Math 101').should('contain','Math 101').click();
        cy.wait(1000);
           cy.contains('New Courseware').should('be.visible').click();
           cy.wait(4000);
        //    cy.get("input[placeholder='Search project']").type('Cypress')
        //    cy.contains('CypressTest').should('contain','CypressTest').click();
        cy.get('button:nth-child(1) div:nth-child(1)').click();
        cy.wait(2000);
        cy.contains('Next').click();
        cy.wait(1000);
        cy.get('button:nth-child(1) div:nth-child(1)').click();
        cy.wait(2000);
        //cy.contains('Deploy').should('be.visible').click();
        cy.get('.styles__InnerButton-sc-9phwpr-5 blXHtF').contains('Deploy').should('be.visible').click();
        cy.wait(1000);
        // cy.get('#more_vert').click();
        // cy.wait(1000);
        // cy.contains('Update Deployment').click();
    }
}
export default Classes;
