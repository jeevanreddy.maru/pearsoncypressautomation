class Courseware {
    navigateToLesson() {
        cy.get("i[id='add']").click();
        cy.wait(1000);
        cy.get("input[placeholder='E.g. Biology']").click();
        cy.wait(1000);
        cy.get("input[placeholder='E.g. Biology']").type('CypressLesson1');
        cy.wait(1000);
        cy.contains('Create project').click();
        cy.wait(1000);
        cy.contains("Content").click();
       // cy.wait(1000);
        cy.xpath("//div[@class='styles__MenuListItemText-sc-17ag5ik-1 hOpRRH'and contains(text(),'Lesson')]").click();
        cy.wait(1000);
    }
}
export default Courseware;