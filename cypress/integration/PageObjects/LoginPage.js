class LoginPage {
    fillEmail(Value) {
        const field = cy.get("input[type='email']");
        field.type(Value);
        return this;
    }
    fillPassword(Value) {
        const field = cy.get("input[type='password']");
        field.clear();
        field.type(Value);
        return this;
    }
    submit() {
        const button = cy.contains('Login');
        button.click();
    }
}
export default LoginPage;
